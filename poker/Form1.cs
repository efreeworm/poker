﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Threading;

namespace poker
{
    public partial class Form1 : Form
    {
        /**
        本次实验内容，采用C#设计，开发工具IDE采用微软官方的产品visual studio 2010版
        实现功能是针对扑克牌的发牌和洗牌，
        A、控制台实现要求
        1、扑克牌的组成：有4种组花色，分别为黑桃、红心、梅花、方块，每种类型有13张牌 分别是A、2-10、J、Q、K，为正牌共52张；另外还有两张特殊的牌，就是大小王，为副牌；本次实验内容不包含大小王的处理
        2、操作顺序：
            a、定义类型、
            b、声明变量
            c、生成52张扑克牌
            d、将52张扑克牌随机分配给4个人，每人13张牌
            e、将每个人手中持有的扑克牌依次在屏幕上显示出来。
        B、在WinForm窗体实现图形化发牌和展示
            1、获取扑克牌图片资源
            2、利用做控制台时实现的对象和方法，来控制初始化52张按顺序排列的扑克牌，可以采用ArrayList的方式来存放52扑克牌
            3、采用随机分配法，将52牌随机分配给4个人，每人手中持有13张扑克牌。
            4、屏幕上放置两个按钮用来“复牌”、“发牌”
            5、点击“复牌”按钮时，屏幕上将52扑克牌的图片按照顺序展示出来。
            6、点击“发牌”按钮时，屏幕上按东西南北四个方向依次把每个人的扑克牌图片展示出来；东、西方向的扑克牌为竖向排列，南北方向的扑克牌为横行排列。
            7、“复牌”、"发牌"按钮要求可以重复点击操作。
         */
        enum Colors { 红心 = 3, 方块 = 5, 梅花 = 4, 黑桃 = 6 };
        enum Point { A = 1, Two , Three , Four , Five , Six, Seven , Eight , Nine , Ten , J, Q, K };
        struct Poker
        {
            public int p1;
            public int p2;
            //构造函数
            public Poker(int p1, int p2)
            {
                this.p1 = p1;
                this.p2 = p2;
            }

            public string Name() {
                return Enum.GetName(typeof(Colors), this.p1) + (this.p2 > 10 || this.p2 ==1 ? ((Point)this.p2).ToString() : this.p2.ToString());
            }
            public void Print()
            {
                Console.Write(" {0}{1} ", ((char)this.p1).ToString(), this.p2);
            }
            public void PrintCn()
            {
                Console.Write(" {0}{1} ", Enum.GetName(typeof(Colors), this.p1), Enum.GetName(typeof(Point), this.p2));
            }
            public void PrintJ()
            {
                Console.Write(" {0}{1} ", ((char)this.p1).ToString(), this.p2 > 10 ? ((Point)this.p2).ToString() : this.p2.ToString());
            }
        }

        //初始化myPoker动态数组变量来存储52张扑克
        ArrayList myPoker = new ArrayList();

        //初始化4个动态数组变量用来存储要发给4个人的扑克
        ArrayList personalOne = new ArrayList();
        ArrayList personalTwo = new ArrayList();
        ArrayList personalThree = new ArrayList();
        ArrayList personalFour = new ArrayList();
        
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int count = this.Controls.Count;
            if (this.Controls.Count > 3)
                for (int i = count-1; i >=0; i--)
                {
                    if(this.Controls[i].Tag!=null)
                    this.Controls.RemoveAt(i);
                }

            myPoker.Clear();
            personalOne.Clear();
            personalTwo.Clear();
            personalThree.Clear();
            personalFour.Clear();
            for (int i = 0; i < 4; i++)
            {
                for (int j = 1; j <= 13; j++)
                {
                    myPoker.Add(new Poker(i + (int)3, j));
                }
            }

            Random r = new Random();
            //开始发牌，一个人一个人的发，每发一张牌得从myPoker中RemoveAt掉扑克牌，Count数减小；第一个人Add到一张牌。
            for (int i = 1; i <= 13; i++)
            {
                int te = r.Next(0, myPoker.Count);
                personalOne.Add(myPoker[te]);
                myPoker.RemoveAt(te);
            }

            //第一个人位于北方 横向显示
            for (int i = 0; i < personalOne.Count; i++)
            {
                PictureBox p = new PictureBox();//实例化一个照片框
                p.BackColor = Color.Blue;
                p.BorderStyle = BorderStyle.FixedSingle;
                Poker pk = (Poker)personalOne[i];
                p.Image = (Image)Properties.Resources.ResourceManager.GetObject("_" + pk.p1 + pk.p2);//Properties.Resources._69;
                p.Tag = pk;
                this.Controls.Add(p);
                p.BringToFront();
                p.Left = 200 +(i*25);
                p.Top = 30;
                p.Width = 78;
                p.Height = 110;
                p.MouseClick += a;
            }

            //分配给第二个人扑克牌
            for (int i = 1; i <= 13; i++)
            {
                int te = r.Next(0, myPoker.Count);
                personalTwo.Add(myPoker[te]);
                myPoker.RemoveAt(te);
            }

            //第二个人位于东方 竖向显示
            for (int i = 0; i < personalTwo.Count; i++)
            {
                PictureBox p = new PictureBox();//实例化一个照片框
                p.BackColor = Color.Blue;
                p.BorderStyle = BorderStyle.FixedSingle;
                Poker pk = (Poker)personalTwo[i];
                p.Image = (Image)Properties.Resources.ResourceManager.GetObject("_" + pk.p1 + pk.p2);//Properties.Resources._69;
                p.Tag = pk;
                this.Controls.Add(p);
                p.BringToFront();
                p.Left = 300 + (13 * 25);
                p.Top = 100 + (i * 30);
                p.Width = 78;
                p.Height = 110;
                p.MouseClick += a;
            }

            //分配给第三个人扑克牌
            for (int i = 1; i <= 13; i++)
            {
                int te = r.Next(0, myPoker.Count);
                personalThree.Add(myPoker[te]);
                myPoker.RemoveAt(te);
            }

            //第三个人位于南方 横向显示
            for (int i = 0; i < personalThree.Count; i++)
            {
                PictureBox p = new PictureBox();//实例化一个照片框
                p.BackColor = Color.Blue;
                p.BorderStyle = BorderStyle.FixedSingle;
                Poker pk = (Poker)personalThree[i];
                p.Image = (Image)Properties.Resources.ResourceManager.GetObject("_" + pk.p1 + pk.p2);//Properties.Resources._69;
                p.Tag = pk;
                this.Controls.Add(p);
                p.BringToFront();
                p.Left = 200 + (i * 25);
                p.Top = 130 + (13 * 30);
                p.Width = 78;
                p.Height = 110;
                p.MouseClick += a;
            }

            //分配给第四个人扑克牌
            for (int i = 1; i <= 13; i++)
            {
                int te = r.Next(0, myPoker.Count);
                personalFour.Add(myPoker[te]);
                myPoker.RemoveAt(te);
            }

            //第四个人位于西方 竖向显示
            for (int i = 0; i < personalFour.Count; i++)
            {
                PictureBox p = new PictureBox();//实例化一个照片框
                p.BackColor = Color.Blue;
                p.BorderStyle = BorderStyle.FixedSingle;
                Poker pk = (Poker)personalFour[i];
                p.Image = (Image)Properties.Resources.ResourceManager.GetObject("_" + pk.p1 + pk.p2);//Properties.Resources._69;
                p.Tag = pk;
                this.Controls.Add(p);
                p.BringToFront();
                p.Left = 60;
                p.Top = 100 + (i * 30);
                p.Width = 78;
                p.Height = 110;
                p.MouseClick += a;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            int count = this.Controls.Count;
            if (count > 3)
                for (int i = count - 1; i >= 0; i--)
                {
                    if (this.Controls[i].Tag != null)
                        this.Controls.RemoveAt(i);
                }
            myPoker.Clear();
            for (int i = 0; i < 4; i++)
            {
                for (int j = 1; j <= 13; j++)
                {
                    myPoker.Add(new Poker(i + (int)3, j));
                }
            }
            //显示按顺序排列的扑克牌
            int col = 0;
            int row = 0;
            for (int i = 0; i < myPoker.Count; i++)
            {
                row = i % 13;
                col = (int)i / 13;
                PictureBox p = new PictureBox();//实例化一个照片框
                p.BackColor = Color.Blue;
                p.BorderStyle = BorderStyle.FixedSingle;
                Poker pk = (Poker)myPoker[i];
                p.Image = (Image)Properties.Resources.ResourceManager.GetObject("_" + pk.p1 + pk.p2);//Properties.Resources._69;
                p.Tag = pk;
                this.Controls.Add(p);
                p.BringToFront();
                p.Left = 100 * col;
                p.Top = 30 * row;
                p.Width = 78;
                p.Height = 110;
                p.MouseClick += a;

                //添加到当前窗口集合中
            }
           
        }
        private void a(object sender, MouseEventArgs e)
        {
            Poker pk = (Poker)((PictureBox)sender).Tag;
            MessageBox.Show(pk.Name());

        }

        /*
         * Assembly myAssem = Assembly.GetEntryAssembly();
         * ResourceManager rm = new ResourceManager("CreateDatabase.Properties.Resources", myAssem);
         * this.pictureBox1.Image = (Image)rm.GetObject("aa");
         */
    }
}
