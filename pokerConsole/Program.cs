﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace pokerConsole
{
    /**
     *  通过C#的方法实现简单的洗牌和发牌
     * （1）52张扑克牌，四种花色（红桃、方块、梅花、黑桃），随机发牌给四个人。
     * （2）最后将四个人的扑克牌包括花色打印在控制台上。
     * （3）通过WinForm窗体来实现在窗口界面显示图形化展示
     *  思考:
     *  (1)输出扑克牌信息时可以按花色排序、按大小排序 
     * 如何实现出牌，以及联网出牌，可以设计一个接龙的纸牌游戏
     */
    class Program
    {
        enum Color { 红心 = 3, 方块 = 4, 梅花 = 5, 黑桃 = 6 };
        enum Point { One=1, Two=2, Three=3, Four=4, Five=5, Six=6, Seven=7, Eight=8, Nine=9, Ten=10, J, Q, K };
        struct Poker
        {
            private int p1;
            private int p2;
            /** 构造函数 */
            public Poker(int p1, int p2)
            {
                this.p1 = p1;
                this.p2 = p2;
            }
            public void Print()
            {
                Console.Write(" {0}{1} ", ((char)this.p1).ToString(), this.p2);
            }
            public void PrintCn()
            {
                Console.Write(" {0}{1} ", Enum.GetName(typeof(Color), this.p1), Enum.GetName(typeof(Point), this.p2));
            }
            public void PrintJ()
            {
                Console.Write(" {0}{1} ", ((char)this.p1).ToString(), this.p2 > 10 ? ((Point)this.p2).ToString() : this.p2.ToString());
            }
        }
        static void Main(string[] args)
        {

            //定义动态数组变量myPoker来存储52张扑克
            ArrayList myPoker = new ArrayList();

            //定义4个动态数组变量用来存储要发给4个人的扑克
            ArrayList personalOne = new ArrayList();
            ArrayList personalTwo = new ArrayList();
            ArrayList personalThree = new ArrayList();
            ArrayList personalFour = new ArrayList();

            //对myPoker进行初始化赋值，共4色，每种颜色为13张扑克
            for (int i = 0; i < 4; i++)
            {
                for (int j = 1; j <= 13; j++)
                {
                    myPoker.Add(new Poker(i + (int)3, j));
                }
            }
            Console.WriteLine("打印所有的扑克");
            for (int i = 0; i < 52; i++)
            {
                if (i % 13 == 0)
                {
                    Console.WriteLine();
                }
                Poker pk = (Poker)myPoker[i];
                pk.Print();
                //pk.PrintCn();
            }


            Random r = new Random();
            //开始发牌，一个人一个人的发，每发一张牌得从myPoker中RemoveAt掉扑克牌，Count数减小；第一个人Add到一张牌。
            for (int i = 1; i <= 13; i++)
            {
                int te = r.Next(0, myPoker.Count);
                personalOne.Add(myPoker[te]);
                myPoker.RemoveAt(te);
            }
            for (int i = 1; i <= 13; i++)
            {
                int te = r.Next(0, myPoker.Count);
                personalTwo.Add(myPoker[te]);
                myPoker.RemoveAt(te);
            }
            for (int i = 1; i <= 13; i++)
            {
                int te = r.Next(0, myPoker.Count);
                personalThree.Add(myPoker[te]);
                myPoker.RemoveAt(te);
            }
            for (int i = 1; i <= 13; i++)
            {
                int te = r.Next(0, myPoker.Count);
                personalFour.Add(myPoker[te]);
                myPoker.RemoveAt(te);
            }

            Console.WriteLine();
            Console.Write("打印第一个人的扑克牌:");
            ShowPokerInfo(personalOne);
            Console.WriteLine();
            Console.Write("打印第二个人的扑克牌:");
            ShowPokerInfo(personalTwo);
            Console.WriteLine();
            Console.Write("打印第三个人的扑克牌:");
            ShowPokerInfo(personalThree);
            Console.WriteLine();
            Console.Write("打印第四个人的扑克牌:");
            ShowPokerInfo(personalFour);

            Console.ReadLine();

        }

        static public void ShowPokerInfo(ArrayList arr){
            for(int i=0;i<13;i++){
                Poker pk = (Poker)arr[i];
                pk.PrintJ();
            }
        }
        
    }
}
