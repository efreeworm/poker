/**
    本次实验内容，采用C#设计，开发工具IDE采用微软官方的产品visual studio 2010版
    实现功能是针对扑克牌的发牌和洗牌，
    1、扑克牌的组成：有4种组花色，分别为黑桃、红心、梅花、方块，每种类型有13张牌 分别是A、2-10、J、Q、K，为正牌共52张；另外还有两张特殊的牌，就是大小王，为副牌；本次实验内容不包含大小王的处理
    2、操作顺序：
        a、定义类型、
        b、声明变量
        c、生成52张扑克牌
        d、将52张扑克牌随机分配给4个人，每人13张牌
        e、将每个人手中持有的扑克牌依次在屏幕上显示出来。

 */
class Program{
    //定义扑克牌的颜色类型
    enum Colors {黑桃,红心,梅花,方块};
    enum Point {A,Two,Three,Four,Five,Six,Seven,Eight,Nine,Ten,J,Q,K};
    struct Poker {
        public int p1,p2;
        public Poker(int p1,int p2){
            this.p1 = p1;
            this.p2 = p2;
        }
        public PrintF(){
            Console.Write("{0},{1}",p1,p2);
        }
    }
    int Main(){
        //定义myPoker动态数组，用来存放52张扑克牌
        ArrayList myPoker = new ArrayList();
        //定义4个动态数组变量，用来存放4个人分别持有的扑克牌
        ArrayList personOne = new ArrayList();
        ArrayList personTwo = new ArrayList();
        ArrayList personThree = new ArrayList();
        ArrayList personFour = new ArrayList();
        //开始初始化52张扑克牌的值
        for(int i=0;i<4;i++){
            for(int j=0;j<13;i++){
                myPoker.Add(new Poker(i, j));
            }
        }
        //将52张扑克牌的值打印到屏幕上
        Console.WriteLn("打印52张扑克牌");
        for(int i=0;i<myPoker.Count;i++){
            myPoker[i].PrintF();
        }
        //开始发牌，一个人一个人的发，取出一张牌后Add(poker),然后将myPoker里已经取出的牌remove
        Random r = new Random();
        for(int i=0;i<13;i++){
            int te = r.Next(0,myPoker.Count);
            personOne.Add(myPoker[te]);
            myPoker.remove(te);
        }
        for(int i=0;i<13;i++){
            int te = r.Next(0,myPoker.Count);
            personTwo.Add(myPoker[te]);
            myPoker.remove(te);
        }
        for(int i=0;i<13;i++){
            int te = r.Next(0,myPoker.Count);
            personThree.Add(myPoker[te]);
            myPoker.remove(te);
        }
        for(int i=0;i<13;i++){
            int te = r.Next(0,myPoker.Count);
            personFour.Add(myPoker[te]);
            myPoker.remove(te);
        }

        //将每个人的扑克牌依次打印到屏幕上显示出来
        for(int i=0;i<personTwo.Count;i++){
            personOne[i].PrintF();
        }

    }
}